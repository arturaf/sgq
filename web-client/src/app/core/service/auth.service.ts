import {Injectable} from '@angular/core';

import {EmptyObservable} from 'rxjs/observable/EmptyObservable';

import {catchError, map} from 'rxjs/operators';

import {BaseService} from '../../core/interface/base.service';
import {NgxPermissionsService} from 'ngx-permissions';
import {NotificationService} from './notification.service';
import {LoginURL, SERVER_URL} from '../../shared/url/url.domain';
import {Observable} from 'rxjs';
import {AppInjector} from '../../app.injector';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ROLE_DOMAIN} from "../domain/user.domain";

export const STORAGE_KEY = 'user';

@Injectable()
export class AuthService extends BaseService {

    private permissionService: NgxPermissionsService = AppInjector.get(NgxPermissionsService);

    private notification: NotificationService = AppInjector.get(NotificationService);

    private translate: TranslateService = AppInjector.get(TranslateService);

    public router: Router = AppInjector.get(Router);
  
    constructor() { 
        super();
    }

    getUser() {
        return JSON.parse(localStorage.getItem(STORAGE_KEY));
    }

    isLogged(): boolean {
        return this.getUser() !== null;
    }

    protected getToken(type: string): string {
        const user = this.getUser();
        if(user) {
            return this.getUser()[type];
        }
        return null;
    }

    getAccessToken(): string {
        return this.getToken('token');
    }

    getRefreshToken(): string {
        return this.getToken('refreshToken');
    }

    saveToken(token: any): void {
        localStorage.setItem(STORAGE_KEY, JSON.stringify(token));
    }

    logout(redirectTo: string = '/login'): void {
        localStorage.removeItem(STORAGE_KEY);
        this.router.navigate([redirectTo]);
    }

    refreshToken(): Observable<string> {
        return this.http.post(SERVER_URL + LoginURL.REFRESH_TOKEN, {token: this.getRefreshToken()})
            .pipe(
                map((result: any) => {
                    this.saveToken(result);
                    let permissions = [...this.getUser().permissions, ...ROLE_DOMAIN[this.getUser().idRole]];
                    if (this.getUser().permissions.length === 0) {
                        permissions = [...permissions, 'update-*', 'delete-*', 'associate-*', 'read-*', 'insert-*'];
                    }
                    this.permissionService.loadPermissions(permissions);
                    return this.getUser().token;
                }),
                catchError((error) => {
                    this.notification.warning(this.translate.instant('system.session.expired'));
                    this.logout();
                    return new EmptyObservable<Response>();
                })
            );
    }    

}
