import {
  throwError as observableThrowError,
  Observable
} from 'rxjs';

import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {catchError} from "rxjs/operators";
import {Router} from "@angular/router";
import {AppInjector} from "../../app.injector";
import {STORAGE_KEY} from "./auth.service";

@Injectable()
export class RequestInterceptorService implements HttpInterceptor {

  public router: Router = AppInjector.get(Router);

  constructor() {}


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    return next.handle(req)
        .pipe(
            catchError(error => {
              if (error instanceof HttpErrorResponse) {
                switch ((<HttpErrorResponse>error).status) {
                  case 401:
                    this.logout();
                    return observableThrowError(error);
                }
              }
              return observableThrowError(error);
            })
        );
  }

  logout(redirectTo: string = '/login'): void {
    localStorage.removeItem('user');
    this.router.navigate([redirectTo]);
  }
}
