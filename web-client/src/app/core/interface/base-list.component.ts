import {Directive, OnInit} from '@angular/core';

import {BaseModelComponent} from './base-model.component';

@Directive()
export abstract class BaseListComponent extends BaseModelComponent implements OnInit {

    public items: any = [];

    protected removeId: any;

    public loading: boolean = true;

    constructor() {
        super();
    }

    ngOnInit(): void {
        super.ngOnInit();

        this.listItems();
    }

    listItems(): void {
        this.loading = true;
        this.service
            .search(
                this.getServiceURL()
            )
            .subscribe(result => {
                    this.items = result;
                    this.loading = false;
                },
                error => {
                    if(error.error) {
                        this.notification.error(error.error.message);
                    }
                    this.loading = false;
                });
    }

    add(): void {
        this.goToAdd();
    }

    edit(id): void {
        this.goToEdit(id);
    }

    view(id): void {
        this.goToView(id);
    }

    remove(id): void {
        this.removeId = id;
        this.delete();
    }

    delete(): void {
        this.service.remove(this.getServiceURL(), this.removeId).subscribe(
            result => {
                this.notification.deleteSuccess();
            },
            error => {
                this.notification.error(
                    error.error ? error.error.message : error.message
                );
            },
            () => {
                /* Finally */
                this.listItems();
            }
        );
    }

    goToAdd(): void {
        this.navigate([this.getRouterURL(), 'create']);
    }

    goToEdit(id = null): void {
        this.navigate([this.getRouterURL(), 'edit', id ? id : '']);
    }

    goToView(id): void {
        this.navigate([this.getRouterURL(), 'view', id]);
    }

    get listIsEmpty(): boolean {
        return this.items.length <= 0;
    }
}
