import {OnInit} from '@angular/core';
import {BaseModelComponent} from './base-model.component';

export abstract class BaseItemComponent extends BaseModelComponent implements OnInit {

  public item: any = new Object();

  constructor() {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.getItem();
  }

  protected getItem(): void {
    const id = this.getParamId();
    if (id) {
      this.service.getOne(this.getServiceURL(), id).subscribe(result => {
        this.item = result;
        this.postGetItem();
      });
    }
  }

  public backToList(): void {
    this.navigate([this.getRouterURL()]);
  }

  protected postGetItem(): void {}

  protected getParamId(): string {
    return this.getParam(this.getItemIdKey());
  }

  protected getItemIdKey(): string {
    return 'id';
  }
}
