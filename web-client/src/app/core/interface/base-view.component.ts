import { OnInit } from '@angular/core';
import { BaseItemComponent } from './base-item.component';

export abstract class BaseViewComponent extends BaseItemComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
  }
}
