import {OnInit} from '@angular/core';

import {BaseComponent} from './base.component';
import {AppInjector} from '../../app.injector';
import {CrudService} from '../service/crud.service';
import {ActivatedRoute} from '@angular/router';

export abstract class BaseModelComponent extends BaseComponent
  implements OnInit {

  protected service: CrudService = AppInjector.get(CrudService);

  constructor() {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  protected getParam(param: string): string {
    return this.getActivatedRoute()
      ? this.getActivatedRoute().snapshot.paramMap.get(param)
      : null;
  }

  protected getActivatedRoute(): ActivatedRoute {
    return null;
  }

  abstract getServiceURL(): string;

  abstract getRouterURL(): string;
}
