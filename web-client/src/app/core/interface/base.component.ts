import {AfterViewInit, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {AppInjector} from '../../app.injector';
import {NotificationService} from '../service/notification.service';
import {TranslateService} from '@ngx-translate/core';

export abstract class BaseComponent implements OnInit, AfterViewInit {

    protected router: Router = AppInjector.get(Router);

    protected notificationService: NotificationService = AppInjector.get(NotificationService);

    protected translateService: TranslateService = AppInjector.get(TranslateService);

    constructor() {}

    ngOnInit(): void {

    }

    ngAfterViewInit(): void {
        
    }

    protected navigate(urls: any[]): void {
        this.router.navigate(urls);
    }

    get notification() {
        return this.notificationService;
    }

    protected translate(code: string): string {
        return this.translateService.instant(code);
    }
}
