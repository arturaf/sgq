import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import {AppInjector} from '../../app.injector';

import {SERVER_URL} from '../../shared/url/url.domain';
import {TranslateService} from '../service/translate.service';

export abstract class BaseService {

    protected http: HttpClient = AppInjector.get(HttpClient);

    protected translateService: TranslateService = AppInjector.get(TranslateService);

    constructor() {}

    get(url, params: HttpParams = null) {
        return this.http.get(SERVER_URL + url, {
            headers: this.getHeaders(),
            params: params
        });
    }

    post(url, body = {}) {
        return this.http.post(SERVER_URL + url, body, {
            headers: this.getHeaders()
        });
    }

    put(url, body = {}) {
        return this.http.put(SERVER_URL + url, body, {
            headers: this.getHeaders()
        });
    }

    patch(url, body = {}) {
        return this.http.patch(SERVER_URL + url, body, {
            headers: this.getHeaders()
        });
    }

    delete(url) {
        return this.http.delete(SERVER_URL + url, { headers: this.getHeaders() });
    }

    protected customHeaders(httpHeaders: HttpHeaders): void { }
    
    protected getHeaders(): HttpHeaders {
        let httpHeaders: HttpHeaders;
        const user = JSON.parse(localStorage.getItem('user'));

        if (user) {
            httpHeaders = new HttpHeaders()
                .set('Authorization', user.token)
                .set('Accept-Language', this.translateService.getLang());
        } else {
            httpHeaders = new HttpHeaders().set(
                'Accept-Language',
                this.translateService.getLang()
            );
        }

        this.customHeaders(httpHeaders);
        return httpHeaders;
    }
    
}
