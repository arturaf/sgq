import {OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {AppInjector} from '../../app.injector';
import {BaseItemComponent} from './base-item.component';
import {isNullOrUndefined} from 'util';

export abstract class BaseEditComponent extends BaseItemComponent implements OnInit {

  protected formBuilder: FormBuilder = AppInjector.get(FormBuilder);

  public editForm: FormGroup;

  public isEditMode: boolean = false;

  public loading: boolean = false;

  constructor() {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.isEditMode = !isNullOrUndefined(this.getParamId());

    this.initForm();
  }

  initForm(): void {
    this.editForm = this.formBuilder.group(this.getFormControls());
  }

  onSubmit(): void {
    this.loading = !this.loading;
    if (!this.item.id) {
      this.insert();
    } else {
      if (this.isUpdatePartial()) {
        this.updatePartial();
      } else {
        this.update();
      }
    }
  }

  protected getFormValue() {
    return this.editForm.value;
  }

  protected insert(): void {
    this.service.insert(this.getServiceURL(), this.getFormValue()).subscribe(
      result => {
        this.notification.insertSuccess();
        this.loading = !this.loading;
        this.backToList();
      },
      error => {
        this.loading = !this.loading;
        this.notification.error(error.error ? error.error.message : error.message);
      }
    );
  }

  protected update(): void {
    this.service
      .update(this.getServiceURL(), this.item.id, this.getFormValue())
      .subscribe(
        result => {
          this.handleUpdate(result);
        },
        error => {
          this.notification.error(
            error.error ? error.error.message : error.message
          );
        }
      );
  }

  protected updatePartial(): void {
    this.service
      .updatePartial(this.getServiceURL(), this.item.id, this.getFormValue())
      .subscribe(
        result => {
          this.handleUpdate(result);
        },
        error => {
          this.loading = !this.loading;
          this.notification.error(error.error ? error.error.message : error.message);
        }
      );
  }

  protected handleUpdate(result): void {
    this.notification.updateSuccess();
    this.loading = !this.loading;
    this.backToList();
  }

  protected postGetItem(): void {
    const formItem = {};
    Object.keys(this.getFormControls()).forEach(key => {
      formItem[key] = this.item[key];
    });
    this.editForm.setValue(formItem);
  }

  protected isUpdatePartial(): boolean {
    return false;
  }

  abstract getFormControls(): Object;

  public equalsSelect(objOne: any, objTwo: any): boolean {
    return objTwo ? objOne.id === objTwo.id : false;
  }
}
