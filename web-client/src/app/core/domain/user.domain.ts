'use strict';

export const ROLES = [
    { key: 'domain.user.super-user', id: 1 }
];

export const ROLE_DOMAIN = {
    1: 'SUPER_USER'
};
