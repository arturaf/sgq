import { Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';

import { LoggedinGuard } from './shared/security/loggedin.guard';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { SecurityUtil } from './core/security/security.util';

export const AppRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      breadcrumb: {
        key: 'home', title: 'home.title', url: '/', icon: 'icon-home2'
      }
    },
    canActivate: [LoggedinGuard],
    canActivateChild: [NgxPermissionsGuard],
    children: [
      {
        path: 'incidents',
        loadChildren: () => import('src/app/components/incident/incident.module').then(m => m.IncidentModule),
        data: {
          breadcrumb: {
            key: 'incident-list', title: 'incident.title.plural', url: 'incidents', icon: 'icon-list', buttons: [
              { url: ['/incidents/create'], label: 'system.new', icon: 'fa fa-plus', permissions: [...SecurityUtil.getPermissionInsert('incident')] }
            ]
          }
        }
      },
      {
        path: 'password/change',
        loadChildren: () => import('src/app/components/change-password/change-password.module').then(m => m.ChangePasswordModule),
        data: {
          breadcrumb: {
            title: 'change-password.label', url: '/password/change', icon: 'icon-lock2'
          }
        }
      }
    ]
  },
  { path: 'login', loadChildren: () => import('src/app/components/login/login.module').then(m => m.LoginModule) },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];
