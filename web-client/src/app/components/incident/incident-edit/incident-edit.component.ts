import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {BaseEditComponent} from '../../../core/interface/base-edit.component';

import {IncidentURL} from '../../../shared/url/url.domain';
import {Validators} from '@angular/forms';

@Component({
    selector: 'app-incident-edit',
    templateUrl: './incident-edit.component.html',
    styleUrls: ['./incident-edit.component.scss']
})
export class IncidentEditComponent extends BaseEditComponent implements OnInit {

    constructor(private route: ActivatedRoute) {
      super();
    }

    ngOnInit() {
      super.ngOnInit();
    }

    getFormControls(): Object {
      return {
        id: this.formBuilder.control(undefined, []),
        standard: this.formBuilder.control(undefined, [Validators.required]),
        consequences: this.formBuilder.control(undefined, [Validators.required]),
        actions: this.formBuilder.control(undefined, [Validators.required])
      };
    }

    getServiceURL() {
      return IncidentURL.BASE;
    }

    getRouterURL() {
      return 'incidents';
    }

    isUpdatePartial(): boolean {
      return true;
    }

    getActivatedRoute(): ActivatedRoute {
      return this.route;
    }
}
