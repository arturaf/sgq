import {Component, OnInit} from '@angular/core';

import {BaseListComponent} from '../../../core/interface/base-list.component';
import {IncidentURL} from '../../../shared/url/url.domain';

@Component({
    selector: 'app-incident-list',
    templateUrl: './incident-list.component.html',
    styleUrls: ['./incident-list.component.scss']
})
export class IncidentListComponent extends BaseListComponent implements OnInit {

    constructor() {
        super();
    }

    ngOnInit() {
        super.ngOnInit();
    }

    getServiceURL(): string {
        return IncidentURL.BASE;
    }

    getRouterURL(): string {
        return 'incidents';
    }
}
