import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {SecurityUtil} from 'src/app/core/security/security.util';
import {SpinnerModule} from 'src/app/core/component/spinner/spinner.module';
import {SimpleSearchModule} from 'src/app/core/component/simple-search/simple-search.module';
import {DeleteConfirmationModule} from 'src/app/core/component/delete-confirmation/delete-confirmation.module';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoadingButtonModule} from 'src/app/core/component/loading-button/loading-button.module';
import {ControlMessageModule} from 'src/app/core/component/control-message/control-message.module';
import {Select2Module} from 'src/app/shared/directive/select2/select2.module';
import {NgxPermissionsModule} from 'ngx-permissions';
import {ButtonChooseModule} from 'src/app/shared/directive/button-choose/button-choose.module';
import {TooltipModule} from 'src/app/shared/directive/tooltip/tooltip.module';

import {IncidentListComponent} from './incident-list/incident-list.component';
import {IncidentEditComponent} from './incident-edit/incident-edit.component';
// import {IncidentViewComponent} from './incident-view/incident-view.component';

const routes: Routes = [
    {
        path: '', component: IncidentListComponent, data: {
            permissions: {
                only:  [...SecurityUtil.getPermissionRead('incident')]
            }
        }
    },
    {
        path: 'create', component: IncidentEditComponent, data: {
            permissions: {
                only: [...SecurityUtil.getPermissionInsert('incident')]
            },
            breadcrumb: {
                key: 'incident-create', title: 'incident.operations.register', icon: 'icon-profile'
            }
        }
    },
    {
        path: 'edit/:id', component: IncidentEditComponent, data: {
            permissions: {
                only: [...SecurityUtil.getPermissionUpdate('incident')]
            },
            breadcrumb: {
                key: 'incident-edit', title: 'incident.operations.update', icon: 'icon-profile'
            }
        }
    },
    // {
    //     path: 'view/:id', component: IncidentViewComponent, data: {
    //         permissions: {
    //             only: [...SecurityUtil.getPermissionRead('incident')]
    //         },
    //         breadcrumb: {
    //             key: 'incident-view', title: 'incident.operations.view', icon: 'icon-profile'
    //         }
    //     }
    // }
];

@NgModule({
    declarations: [IncidentListComponent, IncidentEditComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        RouterModule.forChild(routes),
        RouterModule,
        SpinnerModule,
        SimpleSearchModule,
        DeleteConfirmationModule,
        LoadingButtonModule,
        ControlMessageModule,
        Select2Module,
        ButtonChooseModule,
        TooltipModule,
        NgxPermissionsModule
    ],
    providers: []
})
export class IncidentModule { }
