'use strict';

/**
 * Default URL of the back-end server.
 *
 * @type {string}
 */
export const SERVER_URL = 'http://' + document.location.hostname + ':8080/sgq/';

export namespace LoginURL {
  export const BASE = 'login';
  export const REFRESH_TOKEN = 'refresh';
}

export namespace UserURL {
  export const BASE = 'users';
  export const CHANGE_PASSWORD = BASE + '/current/password';
}

export namespace IncidentURL {
  export const BASE = 'incidents';
}
