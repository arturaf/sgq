package io.artur.controller;

import io.artur.core.controller.CrudBaseController;
import io.artur.core.security.authorization.Authorization;
import io.artur.dto.IncidentDTO;
import io.artur.model.Incident;
import io.artur.service.IncidentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("incidents")
@Authorization("incident")
public class IncidentController extends CrudBaseController<Incident, Integer, IncidentDTO> {

    @Autowired
    private IncidentService service;

    @Override
    protected IncidentService getService() {
        return this.service;
    }
}
