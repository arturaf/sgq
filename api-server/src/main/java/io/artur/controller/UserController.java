package io.artur.controller;

import io.artur.core.controller.CrudBaseController;
import io.artur.core.exception.BusinessException;
import io.artur.core.security.authorization.Authorization;
import io.artur.dto.UserDTO;
import io.artur.model.User;
import io.artur.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * CRUD Controller for the model: User.
 *
 * @author Artur
 */
@RestController
@RequestMapping("users")
@Authorization("user")
public class UserController extends CrudBaseController<User, Integer, UserDTO> {
	
	@Autowired
	private UserService service;
	
	@Override
	protected UserService getService() {
		return service;
	}
	
	@Override
	protected User toModel(UserDTO modelDTO) {
		User model = super.toModel(modelDTO);
		model.setPassword(modelDTO.getNewPassword());
		return model;
	}
	
	@PatchMapping("/current/password")
	public ResponseEntity<?> updateCurrentPassword(@RequestBody UserDTO dto) throws BusinessException {
		getService().updateCurrentPassword(dto.getCurrentPassword(), dto.getNewPassword());
		
		return success();
	}
}
