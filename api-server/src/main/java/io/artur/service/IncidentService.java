package io.artur.service;

import io.artur.core.exception.BusinessException;
import io.artur.core.service.CrudService;
import io.artur.model.Incident;
import io.artur.model.User;
import io.artur.repository.IncidentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class IncidentService extends CrudService<Incident, Integer> {

    @Autowired
    private IncidentRepository repository;

    @Override
    protected IncidentRepository getRepository() {
        return this.repository;
    }

    @Override
    protected Incident preInsert(Incident model) throws BusinessException {
        model.setCreatedAt(new Date());
        model.setReportedBy(new User());
        model.getReportedBy().setId(getCurrentUserId());

        return model;
    }
}
