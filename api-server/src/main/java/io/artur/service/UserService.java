package io.artur.service;

import io.artur.core.exception.BusinessException;
import io.artur.core.service.CrudService;
import io.artur.core.util.CryptoUtil;
import io.artur.model.User;
import io.artur.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Crud Service for model: User.
 *
 * @author Artur
 */
@Service
public class UserService extends CrudService<User, Integer> {

    @Autowired
    private UserRepository repository;

    @Override
    protected UserRepository getRepository() {
        return repository;
    }

    @Override
    public User insert(User user) throws BusinessException {
        user.setPassword(CryptoUtil.hash(user.getPassword()));

        return super.insert(user);
    }
    
    @Override
    protected void validateInsert(User model) throws BusinessException {
    	/* If username already exists */
    	if(getRepository().existsByUsername(model.getUsername())) {
    		throw new BusinessException("user.username.exists");
    	}
    }

    @Override
    protected void validateUpdate(User model) throws BusinessException {
    	/* If username already exists and it is NOT me */
    	if(getRepository().existsByUsernameAndIdNot(model.getUsername(), model.getId())) {
    		throw new BusinessException("user.username.exists");
    	}
    }

    /**
     * Login the user.
     *
     * @param username Username.
     * @param password Password.
     * @return User.
     */
    public User login(String username, String password) {
        return getRepository().login(username, CryptoUtil.hash(password));
    }

    /**
     * Updates the password of the current user.
     * 
     * @param currentPassword
     * 		Current password.
     * @param newPassword
     * 		New password.
     * @throws BusinessException
     */
    @Transactional
    public void updateCurrentPassword(String currentPassword, String newPassword) throws BusinessException {
    	this.updatePassword(getCurrentUserId(), currentPassword, newPassword);
    }
    
    /**
     * Updates the password of the specified user.
     * 
     * @param id
     * 		User ID.
     * @param currentPassword
     * 		Current password.
     * @param newPassword
     * 		New password.
     * @throws BusinessException
     */
    @Transactional
    public void updatePassword(Integer id, String currentPassword, String newPassword) throws BusinessException {
    	if(getRepository().isPasswordCorrect(id, CryptoUtil.hash(currentPassword)) == null) {
    		throw new BusinessException("user.password.wrong");
    	}
    	
    	getRepository().updatePassword(id, CryptoUtil.hash(newPassword));
    }
}
