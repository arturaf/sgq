package io.artur.repository;

import io.artur.core.repository.CrudBaseRepository;
import io.artur.model.Incident;

public interface IncidentRepository extends CrudBaseRepository<Incident, Integer> {


}
