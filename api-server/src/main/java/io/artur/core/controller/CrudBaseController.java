package io.artur.core.controller;

import io.artur.core.dto.ModelDTO;
import io.artur.core.exception.BusinessException;
import io.artur.core.model.Model;
import io.artur.core.security.authorization.DeletePermission;
import io.artur.core.security.authorization.InsertPermission;
import io.artur.core.security.authorization.ReadPermission;
import io.artur.core.security.authorization.UpdatePermission;
import io.artur.core.service.CrudService;
import io.artur.core.util.NullAwareBeanUtils;
import net.jodah.typetools.TypeResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

/**
 * The 'CrudBaseController' class provides the common API for CRUD controllers.
 * <p>
 * If a controller is a model CRUD, this controller MUST extend this class.
 * <p>
 * Provides insertion, update and deletion for the model.
 *
 * @param <M> Model type.
 * @param <T> Model ID type.
 * @param <D> Model DTO type.
 * @author Artur
 */
public abstract class CrudBaseController<M extends Model<T>, T extends Serializable, D extends ModelDTO> extends BaseController {

	protected static final Integer MODEL_INDEX_ORDER = 0;
	protected static final Integer DTO_INDEX_ORDER = 2;

	protected abstract CrudService<M, T> getService();

	/**
	 * Gets one model by its specific ID as DTO.
	 *
	 * @param id ID of instance.
	 * @return DTO of Model instance founded.
	 */
	@GetMapping("/{id}")
	@ReadPermission
	public D getOne(HttpServletRequest request, @PathVariable T id) {
		return toDTO(getOneModel(id));
	}

	@GetMapping
	@ReadPermission
	public List<D> search() {
		List<M> list = this.getService().listAll();

		return toListDTO(list);
	}

	/**
	 * Gets one model by its specific ID as DTO.
	 *
	 * @param id ID of instance.
	 * @return Model instance founded.
	 */
	protected M getOneModel(T id) {
		return getService().getOne(id);
	}

	/**
	 * Inserts the model instance.
	 *
	 * @param modelDTO Model DTO.
	 * @return Response.
	 * @throws BusinessException 
	 */
	@PostMapping
	@InsertPermission
	public ResponseEntity<D> insert(HttpServletRequest request, @Valid @RequestBody D modelDTO) throws BusinessException {
		M model = getService().insert(toModel(modelDTO));
		return created(toDTO(model));
	}

	/**
	 * Updates the model instance.
	 *
	 * @param id       ID of instance.
	 * @param modelDTO Model DTO.
	 * @return Response.
	 * @throws BusinessException 
	 */
	@PutMapping("/{id}")
	@UpdatePermission
	public ResponseEntity<D> update(HttpServletRequest request, @Valid @PathVariable T id, @RequestBody D modelDTO) throws BusinessException {
		getService().update(id, toModel(modelDTO));
		return ok(modelDTO);
	}

	/**
	 * Updates the model instance partially.
	 *
	 * @param id       ID of instance.
	 * @param modelDTO Model.
	 * @return Response.
	 * @throws Exception
	 */
	@PatchMapping("/{id}")
	@UpdatePermission
	public ResponseEntity<D> updatePartial(HttpServletRequest request, @PathVariable T id, @RequestBody D modelDTO) throws Exception {
		M model = toModel(modelDTO);
		M dbModel = getService().getOne(id);
		NullAwareBeanUtils.getInstance().copyProperties(dbModel, model);
		getService().update(id, dbModel);
		return ok(modelDTO);
	}

	/**
	 * Deletes the model instance.
	 *
	 * @param id ID of instance.
	 * @return Response.
	 * @throws Exception
	 */
	@DeleteMapping("/{id}")
	@DeletePermission
	public ResponseEntity<?> delete(HttpServletRequest request, @PathVariable T id) throws BusinessException {
		getService().delete(id);

		return success();
	}

	/**
	 * Response CREATED (201) for the REST requests.
	 *
	 * @param element Element.
	 * @return CREATED (201).
	 */
	protected <E> ResponseEntity<E> created(E element) {
		return new ResponseEntity<>(element, HttpStatus.CREATED);
	}

	/**
	 * Gets the type args of the class.
	 *
	 * @return Type args.
	 */
	protected Class<?>[] getTypeArg() {
		return TypeResolver.resolveRawArguments(CrudBaseController.class, getClass());
	}

	/**
	 * Converts the Model DTO into Model.
	 *
	 * @param modelDTO Model DTO.
	 * @return Model.
	 */
	protected M toModel(D modelDTO) {
		return mapTo(modelDTO, getModelClass());
	}

	/**
	 * Converts the Model into Model DTO.
	 *
	 * @param model Model.
	 * @return Model DTO.
	 */
	protected D toDTO(M model) {
		return mapTo(model, getDTOClass());
	}

	/**
	 * Converts the list of models into list of DTOs.
	 *
	 * @param items Model items.
	 * @return DTOs.
	 */
	protected List<D> toListDTO(List<?> items) {
		return toList(items, getDTOClass());
	}

	/**
	 * Gets the Model class.
	 *
	 * @return Model class.
	 */
	@SuppressWarnings("unchecked")
	protected Class<M> getModelClass() {
		return (Class<M>) getTypeArg()[MODEL_INDEX_ORDER];
	}

	/**
	 * Gets the DTO class.
	 *
	 * @return DTO class.
	 */
	@SuppressWarnings("unchecked")
	protected Class<D> getDTOClass() {
		return (Class<D>) getTypeArg()[DTO_INDEX_ORDER];
	}
}
