package io.artur.core.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.artur.core.config.AppContext;
import io.artur.core.dto.LoggedUserDTO;
import io.artur.core.util.JSonUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

/**
 * Filter for Login.
 *
 * @author Artur
 */
public class LoginFilter extends AbstractAuthenticationProcessingFilter {

    /**
     * Constructor.
     *
     * @param url URL.
     */
    public LoginFilter(String url) {
        super(new AntPathRequestMatcher(url));

        this.setAuthenticationManager(new JWTAuthenticationManager());
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        SecurityUtils.fillAccessControlHeader(response);

        AccountCredentials creds = new ObjectMapper().readValue(request.getInputStream(), AccountCredentials.class);

        return this.getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(creds.getUsername(),
                creds.getPassword(), Collections.emptyList()));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication auth) throws IOException {

        LoggedUserDTO dto = (LoggedUserDTO) auth.getPrincipal();

        dto = getTokenService().addAuthentication(dto);

        response.getWriter().write(JSonUtil.toJSon(dto));
    }

    /**
     * Gets the token service.
     * 
     * @return Token Service.
     */
    private TokenAuthenticationService getTokenService() {
    	return AppContext.getBean(TokenAuthenticationService.class);
    }
}
