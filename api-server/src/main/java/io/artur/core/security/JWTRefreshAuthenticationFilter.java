package io.artur.core.security;

import io.artur.core.config.AppContext;
import io.artur.core.dto.LoggedUserDTO;
import io.artur.core.util.JSonUtil;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter for Refresh Authentication.
 *
 * @author Artur
 *
 */
public class JWTRefreshAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

	/**
	 * Constructor.
	 *
	 * @param url URL.
	 */
	public JWTRefreshAuthenticationFilter(String url) {
		super(new AntPathRequestMatcher(url));
		
		this.setAuthenticationManager(new JWTAuthenticationManager());
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {

		response = SecurityUtils.fillAccessControlHeader(response);
		
		return getTokenService().getRefreshAuthetication(request);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication auth) throws IOException, ServletException {

		LoggedUserDTO dto = getTokenService().addAuthentication((LoggedUserDTO) auth.getPrincipal());
		dto = getManager().addPermissions(dto, dto.getIdRole());
		
		response.setHeader("Content-Type", "application/json");
		response.getWriter().write(JSonUtil.toJSon(dto));
	}

	/**
	 * Gets the token service.
	 * 
	 * @return Token Service.
	 */
	private TokenAuthenticationService getTokenService() {
		return AppContext.getBean(TokenAuthenticationService.class);
	}
	
	/**
	 * Gets the JWT Authentication Manager.
	 * 
	 * @return JWT Authentication Manager.
	 */
	private JWTAuthenticationManager getManager() {
		return (JWTAuthenticationManager) getAuthenticationManager();
	}
}
