package io.artur.core.config;

import io.artur.core.dto.RestMessageDTO;
import io.artur.core.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handler for REST Exceptions.
 *
 * @author Artur
 */
@ControllerAdvice
public class RestExceptionHandler {

    private static final String UNEXPECTED_ERROR = "exception.unexpected";
    private static final Logger LOGGER = Logger.getLogger(RestExceptionHandler.class.getName());
    private final MessageSource messageSource;

    /**
     * Constructor.
     *
     * @param messageSource Message Source.
     */
    @Autowired
    public RestExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    /**
     * Handler for: Business Exception.
     *
     * @param ex     Exception.
     * @param locale Locale.
     * @return Message response.
     */
    @ExceptionHandler(value = BusinessException.class)
    public ResponseEntity<RestMessageDTO> handleBusinessExpecption(BusinessException ex) {
        return new ResponseEntity<>(new RestMessageDTO(ex.getMessage(), ex.getCode()), HttpStatus.CONFLICT);
    }

    /**
     * Handler for: All Exceptions.
     *
     * @param ex     Exception.
     * @param locale Locale.
     * @return Message response.
     */
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<RestMessageDTO> handleExceptions(Exception ex) {
        String errorMessage = messageSource.getMessage(UNEXPECTED_ERROR, null, LocaleContextHolder.getLocale());
        LOGGER.log(Level.ALL, ex.toString(), ex);
        return new ResponseEntity<>(new RestMessageDTO(errorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}