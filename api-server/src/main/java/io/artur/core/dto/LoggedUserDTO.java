package io.artur.core.dto;

import java.util.List;

/**
 * DTO for Logged User.
 *
 * @author Artur
 */
public class LoggedUserDTO {

	private Integer id;
	
    private String name;

    private String username;

    private Integer idRole;

    private String token;

    private String refreshToken;
    
    private List<String> permissions;

    public LoggedUserDTO() {
	}
    
    /**
     * Constructor.
     * 
     * @param id
     * @param username
     */
    public LoggedUserDTO(Integer id, String username) {
		super();
		
		this.id = id;
		this.username = username;
	}
    
    /**
     * Constructor.
     * 
     * @param id
     * @param username
     * @param idRole
     */
    public LoggedUserDTO(Integer id, String username, Integer idRole) {
    	super();
    	
    	this.id = id;
    	this.username = username;
    	this.idRole = idRole;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
    
    public Integer getIdRole() {
		return idRole;
	}

	public void setIdRole(Integer idRole) {
		this.idRole = idRole;
	}

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

	public List<String> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<String> permissions) {
		this.permissions = permissions;
	}
}
