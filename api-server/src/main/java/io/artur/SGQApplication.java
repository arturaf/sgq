package io.artur;

import io.artur.core.config.AppContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Main class of application.
 * 
 * @author Artur
 */
@SpringBootApplication
@EnableCaching
@EnableWebMvc
public class SGQApplication {

	/**
	 * Main.
	 * 
	 * @param args
	 * 		Arguments.
	 */
	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(SGQApplication.class, args);
		
		AppContext.loadApplicationContext(ctx);
	}
}
