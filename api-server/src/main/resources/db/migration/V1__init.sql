/*------------------------CREATE TABLES------------------------*/
CREATE TABLE "role" (
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(50) NOT NULL,
	description VARCHAR(250) NULL
);

CREATE TABLE user_account (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  username VARCHAR(255) NOT NULL,
  password VARCHAR(100) NOT NULL,
  roleid INTEGER NOT NULL,
  CONSTRAINT user_role_FK FOREIGN KEY (roleid) REFERENCES "role" (id)
);

CREATE TABLE permission (
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(50) NOT NULL,
	description VARCHAR(250) NULL
);

CREATE TABLE role_permission (
	idrole INTEGER NOT NULL,
	idpermission INTEGER NOT NULL,
	PRIMARY KEY (idrole, idpermission),
	CONSTRAINT role_permission_role_FK FOREIGN KEY (idrole) REFERENCES "role" (id),
	CONSTRAINT role_permission_permission_FK FOREIGN KEY (idpermission) REFERENCES permission (id)
);

CREATE TABLE "incident" (
	id SERIAL NOT NULL PRIMARY KEY,
	reported_by INTEGER NOT NULL,
	created_at DATE NOT NULL,
	standard VARCHAR(50) NOT NULL,
	consequences VARCHAR(500) NOT NULL,
	actions VARCHAR(500) NOT NULL,
	observation VARCHAR(500),
	CONSTRAINT incident_user_FK FOREIGN KEY (reported_by) REFERENCES user_account (id)
);

/*------------------------INSERT VALUES------------------------*/

INSERT INTO "role" (id, name, description) VALUES (1, 'Super User', 'Role for a super user that allows all operations in the system.');

INSERT INTO user_account (name,username,password,roleid) VALUES ('Artur Farias','artur.farias','4f26aeafdb2367620a393c973eddbe8f8b846ebd', 1);

INSERT INTO permission (id, name, description) VALUES (1, 'read-*', 'Read all items.'), (2, 'insert-*', 'Insert any item.'), (3, 'update-*', 'Update any item.'), (4, 'delete-*', 'Delete any item.'), (5, 'associate-*', 'Associate any item to other item');

INSERT INTO role_permission (idrole, idpermission) VALUES (1, 1), (1, 2), (1, 3), (1, 4), (1, 5);

INSERT INTO incident (id, reported_by, created_at, standard, consequences, actions, observation) VALUES (2, 1, CURRENT_DATE, 'ATF 1644', 'O automóvel pode apresentar superaquecimento devido à temperatura da peça que compõe o sistema de freios.', '1. Devolver a peça ao laboratório. 2. Utilizar a versão antiga do kit de freios.', NULL);